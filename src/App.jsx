import React, { useState } from 'react';
import { CounterViewer, CounterButtons, TextChanger } from './components';
import './App.scss';

function App() {
    const [count, setcount] = useState(0);
    const [sharedText, setSharedText] = useState('');

    const countUp = (quantity = 1) => {
        setcount(count + quantity);
    };

    const countDown = (quantity = 1) => {
        if (count > 0) setcount(count - quantity);
    };

    const changeText = (newText) => {
        setSharedText(newText);
    };

    return (
        <div className="app">
            <CounterViewer count={count} />
            <CounterButtons countUp={() => countUp()} countDown={() => countDown()} text="Up or down 1" />
            <CounterButtons countUp={() => countUp(5)} countDown={() => countDown(5)} text="Up or down 5" />
            <CounterButtons countUp={() => countUp(10)} countDown={() => countDown(10)} text="Up or down 10" />
            <h1>Text:</h1>
            <h3>{sharedText || 'Default text'}</h3>
            <TextChanger changeText={changeText} />
        </div>
    );
}

export default App;

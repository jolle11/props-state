import { useState } from 'react';

const TextChanger = (props) => {
    const [text, setText] = useState('');
    const changeText = () => {
        props.changeText(text);
        setText('');
    };
    const inputChange = (event) => {
        setText(event.target.value);
    };
    return (
        <>
            <input type="text" onChange={inputChange} value={text} />
            <button onClick={changeText}>Change Text</button>
        </>
    );
};

export default TextChanger;

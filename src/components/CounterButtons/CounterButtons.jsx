const CounterButtons = (props) => {
    return (
        <div>
            <h2>{props.text}</h2>
            <button onClick={props.countUp}>+</button>
            <button onClick={props.countDown}>-</button>
        </div>
    );
};

export default CounterButtons;

const CounterViewer = (props) => {
    return <h1 className={props.count < 10 ? 'red' : 'blue'}>Counter: {props.count}</h1>;
};

export default CounterViewer;
